import React from "react";
import { useState, useEffect } from "react";
import "./dashboard.css";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { styled } from "@mui/material/styles";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import InputBase from "@mui/material/InputBase";
import EditIcon from "@mui/icons-material/Edit";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Typography, Button, Card } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { removeUserSession } from "../../utils/common";
import { useNavigate } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import { getToken } from "../../utils/common";
import validator from "validator";
import { toast, ToastContainer } from "react-toastify";
import axios from "axios";

const currentDate = new Date();

const getDate = (str) => {
  if (str !== undefined) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  } else {
    return "";
  }
};

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const useStyles = makeStyles(() => ({
  title: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    borderRadius: 3,
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    color: "white",
    padding: "0 30px",
  },
  field: {
    width: "90%",
    "&&": {
      marginBottom: "10px",
    },
  },
}));

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
  p: 4,
};

const Search = styled("div")(() => ({
  position: "relative",
  backgroundColor: "#f5f5f5",
  marginLeft: 0,
  marginRight: 2,
  width: "100%",
}));

const SearchIconWrapper = styled("div")(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: "100%",
  position: "absolute",
  pointerEvents: "none",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: "inherit",
  "& .MuiInputBase-input": {
    padding: theme.spacing(2, 2, 2, 0),
    paddingLeft: `calc(1em + ${theme.spacing(3)})`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "25ch",
    },
  },
}));

const columns = {
  id: "fullName",
  label1: "Name",
  label2: "Email",
  label3: "Employee Code",
  label4: "Department",
  label5: "Position",
  label6: "Date of joining",
  // label: "Action",
};

export default function Dashboard() {
  const navigate = useNavigate();
  const classes = useStyles();
  const token = getToken();
  const [userData, setUserData] = useState({
    fullName: "",
    email: "",
    empCode: "",
    department: "",
    position: "",
    dateOfJoin: "",
    image: "",
  });
  const [validation, setValidation] = useState(false);
  const [emailvalidate, setIsEmailValidate] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  const [count, setCount] = useState(0);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    setOpen(false);
    navigate("/dashboard");
  };

  const handleLogout = () => {
    removeUserSession();
    navigate("/");
  };

  const handleChange = (e) => {
    if (e.target.name === "email") {
      setIsEmailValidate(validator.isEmail(e.target.value));
    }
    setUserData((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const handleSubmit =  (e) => {
    // try {
    //   e.preventDefault();
    //   if (!emailvalidate) {
    //     toast.error("Please enter valid email");
    //   } else {
    //     const data = await fetch("http://10.20.1.225:8001/api/register", {
    //       method: "POST",
    //       headers: { "content-type": "application/json" },
    //       body: JSON.stringify(userData),
    //     })
    //     console.log("Data----------->", data , "data")
    //   }
    // } catch (error) {
    //   console.log("error--------->", error);
    //   toast.error(error.message); 
    // }
    e.preventDefault();
    if (!emailvalidate) {
      toast.error("Please enter valid email");
    } else {
      fetch("http://10.20.1.225:8001/api/register", {
        method: "POST",
        headers: { "content-type": "application/json" },
        body: JSON.stringify(userData),
      })
        .then(async (res) => {
          if (!res.ok) {
            console.log(res.json());
          }
          return await res.json();
        })
        .then((res) => {
          setUserData({
            fullName: "",
            email: "",
            empCode: "",
            department: "",
            position: "",
            dateOfJoin: "",
            image: "",
          });
          handleClose();
          setCount(count + 1);
        })
        .catch(async (error) => {
          console.log("error--------->", error);
          toast.error(error.message);
        });
    }
  };

  useEffect(() => {
    fetch(`http://10.20.1.225:8001/api/user-list/`, {
      headers: { Authorization: `Bearer ${token}` },
    })
      .then((res) => {
        return res.json();
      })
      .then((response) => {
        setTableData(response.data);
      })
      .catch((error) => {
        toast.error(error.message);
      });
  }, [count]);

  return (
    <div>
      <nav>
        <div className="logo">Co-hand</div>
        <ul>
          <li>
            <span>Welcome to Admin Portal</span>
          </li>
          <li>
            <a href="#" className="active btn_user" onClick={handleOpen}>
              Create User
            </a>
          </li>
        </ul>
        <Button
          onClick={handleLogout}
          style={{
            fontSize: "16px",
            backgroundColor: "blue",
            fontFamily: "Oswald",
            letterSpacing: "2px",
            color: "#fff",
          }}
        >
          Logout
        </Button>
      </nav>
      <div>
        <Typography
          className={classes.title}
          variant="h2"
          component="h1"
          gutterBottom
          align="center"
          style={{
            fontFamily: "Poppins",
            letterSpacing: "4px",
            fontSize: "2.75rem",
            lineHeight: "1.3",
          }}
        >
          Current Users
        </Typography>
        <Card>
          <Search>
            <StyledInputBase
              placeholder="Search…"
              inputProps={{ "aria-label": "search" }}
              value={searchInput}
              onChange={(e) => setSearchInput(e.target.value)}
            />
          </Search>
          <TableContainer sx={{ maxHeight: 380 }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <StyledTableCell>
                    <Typography>{columns.label1}</Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography>{columns.label2}</Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography>{columns.label3}</Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography>{columns.label4}</Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography>{columns.label5}</Typography>
                  </StyledTableCell>
                  <StyledTableCell>
                    <Typography>{columns.label6}</Typography>
                  </StyledTableCell>
                  {/* <StyledTableCell>
                    <Typography>{columns.label}</Typography>
                  </StyledTableCell> */}
                </TableRow>
              </TableHead>
              <TableBody>
                {tableData
                  .filter((value) => {
                    if (searchInput === "") {
                      return value;
                    } else if (
                      value.email
                        .toLowerCase()
                        .includes(searchInput.toLowerCase()) ||
                      value.empCode
                        .toLowerCase()
                        .includes(searchInput.toLowerCase())
                    ) {
                      return value;
                    }
                  })

                  .map((row, i) => {
                    return (
                      <TableRow key={i}>
                        <StyledTableCell>{row.fullName}</StyledTableCell>
                        <StyledTableCell>{row.email}</StyledTableCell>
                        <StyledTableCell>{row.empCode}</StyledTableCell>
                        <StyledTableCell>{row.department}</StyledTableCell>
                        <StyledTableCell>{row.position}</StyledTableCell>
                        <StyledTableCell>
                          {row.dateOfJoin?.split("T")[0]}
                        </StyledTableCell>
                        {/* <StyledTableCell>
                          <EditIcon onClick={handleEdit} />
                          <DeleteOutlineOutlinedIcon onClick={handleDelete} />
                        </StyledTableCell> */}
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
        </Card>
      </div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <form onSubmit={handleSubmit}>
            <Typography
              variant="h2"
              component="h1"
              color="error"
              gutterBottom
              align="center"
              style={{ fontFamily: "Oswald" }}
            >
              ADD USER
            </Typography>
            <div className="field">
              <TextField
                className={classes.field}
                required
                onMouseDown={(e) => setValidation(true)}
                name="fullName"
                onChange={handleChange}
                id="outlined-basic"
                label="Enter full name"
                variant="outlined"
              />
              <TextField
                className={classes.field}
                required
                name="email"
                onChange={handleChange}
                id="outlined-basic"
                type="email"
                label="Enter email"
                variant="outlined"
              />
              <TextField
                className={classes.field}
                required
                name="empCode"
                type="number"
                onChange={handleChange}
                id="outlined-basic"
                label="Enter employee code"
                variant="outlined"
              />
              <TextField
                className={classes.field}
                required
                name="department"
                onChange={handleChange}
                id="outlined-basic"
                label="Enter department"
                variant="outlined"
              />
              <TextField
                className={classes.field}
                required
                name="position"
                onChange={handleChange}
                id="outlined-basic"
                label="Enter position"
                variant="outlined"
              />
              <TextField
                className={classes.field}
                required
                id="date"
                name="dateOfJoin"
                type="date"
                // defaultValue=""
                onChange={handleChange}
                label="Enter date of joining"
                InputLabelProps={{
                  shrink: true,
                }}
                inputProps={{ min: "2019-01-24", max: getDate(currentDate) }}
              />
              <div className="btn">
                <Button
                  type="submit"
                  variant="contained"
                  color="success"
                  style={{ marginLeft: "-12px ", fontFamily: "Oswald" }}
                >
                  Save
                </Button>
                <Button
                  onClick={handleClose}
                  variant="contained"
                  style={{ marginLeft: "20px", fontFamily: "Oswald" }}
                >
                  Back
                </Button>
              </div>
            </div>
          </form>
        </Box>
      </Modal>

      <ToastContainer />
    </div>
  );
}
