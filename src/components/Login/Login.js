import React from "react";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { setUserSession } from "../../utils/common";
import "./login.css";
import axios from "axios";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Login = (props) => {
  const navigate = useNavigate();
  const user = useFormInput("");
  const password = useFormInput("");
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleLogin = () => {
    setError(null);
    setLoading(true);
    axios
      .post("http://10.20.1.225:8001/api/login", {
        user: user.value,
        password: password.value,
      })
      .then((response) => {
        setLoading(false);
        if (response.data.data.role == "admin") {
          setUserSession(response.data.data.user, response.data.data.token);
          navigate("/dashboard");
        } else {
          toast.error("Unauthorised access");
        }
      })
      .catch((error) => {
        setLoading(false);
        if (error.response.data.code === 404)
          setError(error.response.data.message);
        else setError("Something went wrong. Please try again later.");
      });
  };
  return (
    <div>
      <section className="vh-100">
        <div className="container-fluid h-custom">
          <div className="row d-flex justify-content-center align-items-center h-100">
            <div className="col-md-9 col-lg-6 col-xl-5">
              <img
                src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
                className="img-fluid"
                alt=""
              />
            </div>
            <div className="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
              <form onSubmit={handleLogin}>
                <div className="divider d-flex align-items-center my-4">
                  <p className="heading head">Hello Admin</p>
                </div>
                <div className="form-outline mb-4">
                  <input
                    className="form-control form-control-lg input1"
                    type="text"
                    {...user}
                    autoComplete="new-password"
                    placeholder="Enter User"
                  />
                </div>
                <div className="form-outline mb-3">
                  <input
                    className="form-control form-control-lg input2"
                    type="password"
                    {...password}
                    autoComplete="new-password"
                    placeholder="Enter password"
                  />
                </div>
                {error && (
                  <>
                    <small style={{ color: "red" }}>{error}</small>

                    <br />
                  </>
                )}
                {/* <div className="d-flex justify-content-between align-items-center">
                  <a href="#!" className="text-body">
                    Forgot password?
                  </a>
                </div> */}

                <a
                  className="btn btn-primary btn-lg login"
                  type="button"
                  value={loading ? "Loading..." : "Login"}
                  onClick={handleLogin}
                  disabled={loading}
                >
                  Login
                </a>
              </form>
            </div>
          </div>
        </div>
      </section>
      <ToastContainer />
    </div>
  );
};

const useFormInput = (initialValue) => {
  const [value, setValue] = useState(initialValue);

  const handleChange = (e) => {
    setValue(e.target.value);
  };
  return {
    value,
    onChange: handleChange,
  };
};

export default Login;
